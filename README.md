Laundry Kiosk
=============

Kiosk project for laundry in new campus.

Python tkinter project depends on NT Service (internal 3rdparty card system)
and external API for laundry machine payment by vendor.

Currently, kiosk uses `Task Scheduler` to start VcXsrv and python application
on startup.

Architecture
------------

    +---------------+     +-----+     +---------+
    | laundry-kiosk | --> | API | <-> | laundry |
    +---------------+     +-----+     +---------+
            ^
            |          +----+     +----------------------+     +-------------+
            +--------> | DB | <-> | NT Service (payment) | <-> | card reader |
                       +----+     +----------------------+     +-------------+

Install
-------

1. Install and configure database and NT service on the machine.

  - Allow remote connections
  - Add new user to access the database in config
  - Enable TCP login for 127.0.0.1 as config with SQL Server Authentication

2. Setup [WSL][wsl] and Ubuntu on windows.

3. Install [VcXsrv](https://sourceforge.net/projects/vcxsrv/).

4. Install the source code and its dependencies in bash.

    $ sudo apt update
    $ sudo apt install python3-venv python3-dev python3-tk unixodbc-dev gcc g++ tdsodbc

5. Install `laundry-kiosk` preferably in `$HOME` (`~`).

    $ git clone https://bitbucket.org/ctiteam/laundry-kiosk
    $ cd laundry-kiosk
    $ python3.8 -m venv venv
    $ source venv/bin/activate
    (venv) $ pip install -r requirements.txt

6. Configure UnixODBC and FreeTDS.

    # /etc/freetds/freetds.conf
    [localhost]
        host = 127.0.0.1
        port = 1433
        tds version = 8.0

    # /etc/odbcinst.ini
    [FreeTDS]
    Decription = FreeTDS
    Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so

    # /etc/odbc.ini
    [localhost]
    Driver = FreeTDS
    Description = Local MsSQL database
    ServerName = localhost
    Database = master

7. Configure `laundry/config.py` and add `secret.key`.

8. Run `laundry-kiosk` (note that VcXsrv needs to be started).

    $ cd laundry-kiosk
    $ source venv/bin/activate
    (venv) $ python -m laundry

9. Configure `Task Scheduler` to auto start VcXsrv on login by starting it with
   `"C:\\Program Files\VcXsrv\vcxsrv.exe" :0 -fullscreen`, disable stop the task
   if it runs longer than.

10. Configure `Task Scheduler` to auto start `laundry-kiosk` on login
   preferably with delay and auto-update with
   `C:\\Windows\System32\bash.exe -c "cd ~/laundry-kiosk; git pull; source venv/bin/activate; while :; do DISPLAY=:0 python -m laundry; done"`,
   disable stop the task if it runs longer than.

11. Optional, activate windows to enable auto login.

[wsl]: https://docs.microsoft.com/en-us/windows/wsl/install-win10
