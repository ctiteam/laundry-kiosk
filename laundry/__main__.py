"""Laundry Tkinter GUI."""
import logging
import sys
import tkinter
import tkinter.font
import tkinter.ttk
import typing

from . import api
from . import db

import tkinter.font as fnt

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger(__name__)

root = tkinter.Tk()
root.title("Laundry")
root.attributes('-fullscreen', True)
root.pack_propagate(False)  # never let children set geometry
width, height = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry(f"{width}x{height}")
root.resizable(False, False)
root.overrideredirect(True)  # prevent it from closing
root.focus_set()

large_font = tkinter.font.Font(root, size=30)
small_font = tkinter.font.Font(root, size=10)
style = tkinter.ttk.Style()
style.configure('TRadiobutton', padding=20, indicatorrelief='', indicatoron=0,
                font=small_font)
style.configure('TButton', font=small_font, padding=30)
style.configure('TLabel', font=large_font)

# Frame stacking
container = tkinter.ttk.Frame(root)
container.pack(expand=1, fill='both')
container.grid_rowconfigure(0, weight=1)
container.grid_columnconfigure(0, weight=1)


def self_destruct(t: int, label: tkinter.ttk.Label, frame: tkinter.ttk.Frame, buttonFrame: tkinter.ttk.Frame):
    """
    Self destruct and go back to menu within t seconds.
    Update label for timing, destroy frame on complete.
    Keep root.timer_job for job, remember to cancel it.
    """
    def tick():
        root.timer -= 1
        if root.timer:
            try:
                text = label['text'].split(' (')[0]
                label.configure(text=f"{text} (closes in {root.timer}s)")
                root.timer_job = root.after(1000, tick)
            except tkinter.TclError as error:
                pass
        else:
            #frame.destroy()
            closeScreen(buttonFrame,frame)
            del root.timer

    root.timer = t + 1
    root.timer_job = root.after(0, tick)


def genchoice(action: str):
    """List of machines based on type of action."""
    assert action in {'wash', 'dry'}

    def wrapper():
        buttonframe = tkinter.ttk.Frame(container)
        frame = tkinter.ttk.Frame(container)
        frame.grid(row=0, column=0, sticky='nsew')
        buttonframe.grid(row=1, column=0, sticky='nsew')
        closebutton = tkinter.Button(buttonframe, text="Back to Main", font=fnt.Font(size=50),
                                     command=lambda: closeScreen(buttonframe, frame))
        closebutton.pack()
        label = tkinter.ttk.Label(frame, text="Select Machine")
        label.pack(side='top', ipady=40)
        chosen = tkinter.StringVar(frame)
        initial = 'W' if action == 'wash' else 'D'

        machines = {k: v for k, v in api.status().items()
                    if v['type'].startswith(initial)}

        for key,value in machines.items():
            print(key,value)


        logger.debug(f"{machines=}")
        for machine in machines.values():
            tkinter.ttk.Radiobutton(
                frame, text=f"Machine {machine['machineName']}",
                value=machine['machineName'], variable=chosen,
                state="enable" if (not machine['running'] and machine['online'] == True) or
                                  (action == 'dry' and not machine['running'] and machine['online'] == True) # extra
                      else "disabled").pack(side='left', expand=1, fill='both')
        self_destruct(30, label, frame, buttonframe)  # start 1st self destruct
        frame.wait_variable(chosen)
        #root.after_cancel(root.timer_job)  # stop 1st self destruct
        machine = next(machineid for machineid, machine in machines.items()
                       if machine['machineName'] == chosen.get())
        frame.destroy()
        closeScreen(buttonframe)

        options(action, machine, machines[machine]['price'],
                machines[machine]['online'])
    return wrapper


def options(action: str, machine: str, choices: typing.Dict[str, str],
            online: bool):  # online is only used for dry
    """Generate options for pricing using different options."""
    def pay():
        root.after_cancel(root.timer_job)  # stop 2nd self destruct
        top = tkinter.Toplevel()
        top.attributes('-type', 'dialog')
        top.geometry(f"400x200+{int(width/2) - 200}+{int(height/2) - 100}")
        if action == 'wash':
            amount = float(choices[chosen.get()])
            logger.debug(f"{machine=} {chosen.get()=} {amount=}")
        else:
            amount = root.price
            del root.price
            logger.debug(f"{machine=} {amount=}")
        try:
            db.pay(amount)
            api.pay(amount, machine)
            text = "Success"
        except ValueError as err:
            text = f"Failure - {err}"
        top.title(text)
        cnf = dict(font=large_font)
        tkinter.Message(top, cnf=cnf, text=text, padx=20, pady=20).pack()
        top.after(1000, top.destroy)
        frame.destroy()
        closeScreen(buttonFrame, None)

    frame = tkinter.ttk.Frame(container)
    frame.grid(row=0, column=0, sticky='nsew')
    inner_frame = tkinter.ttk.Frame(frame)
    buttonFrame = tkinter.ttk.Frame(container)
    buttonFrame.grid(row=1, column=0, sticky='nsew')

    closebutton = tkinter.Button(buttonFrame, text="Back to Main", font=fnt.Font(size=50),
                                 command=lambda: closeScreen(buttonFrame,frame))
    closebutton.pack()

    if action == 'wash':
        label = tkinter.ttk.Label(frame, text="Select Mode")
        chosen = tkinter.StringVar(frame)
        for choice, value in choices.items():
            price = float(value)
            tkinter.ttk.Radiobutton(
                inner_frame, text=f"{choice} (RM {price:.2f})", value=choice,
                variable=chosen).pack(side='left', expand=1, fill='both')
    else:  # dry
        root.price = 0
        minprice = float(choices['minPrice'] or 0) if online else 0.5
        maxprice = choices['maxPrice'] and float(choices['maxPrice'])  # None
        runtime = float(choices['runTime'])

        def update(value):
            def wrapper():
                root.price += value
                price_label.configure(
                    text=f"Price: RM {root.price:.2f}\n"
                         f"Duration: {int(root.price * runtime)} minutes")
                # price limit
                minus_button.configure(
                    state='disabled' if root.price == minprice else 'enable')
                plus_button.configure(
                    state='disabled' if root.price == maxprice else 'enable')

            return wrapper

        label = tkinter.ttk.Label(frame, text="Select Duration")
        inner_frame = tkinter.ttk.Frame(frame)
        minus_button = tkinter.ttk.Button(inner_frame, text="-",
                                          command=update(-0.5))
        minus_button.pack(side='left', expand=1, fill='both')
        price_label = tkinter.ttk.Label(inner_frame)
        price_label.pack(side='left', expand=1, fill='both', padx=40)
        plus_button = tkinter.ttk.Button(inner_frame, text="+",
                                         command=update(+0.5))
        plus_button.pack(side='left', expand=1, fill='both')
        update(minprice)()  # default price

    label.pack(side='top', ipady=40)
    inner_frame.pack(expand=1, fill='both')
    self_destruct(30, label, frame,buttonFrame)  # start 2nd self destruct
    if action == 'wash':
        # wait for user to choose before displaying pay button
        frame.wait_variable(chosen)
    button = tkinter.ttk.Button(frame, text="Pay", command=pay)
    button.pack(fill='both')


def closeScreen(buttonFrame,frame=None):

    if buttonFrame:
        deleteChildren(buttonFrame)
        buttonFrame.destroy()
    if frame:
        deleteChildren(frame)
        frame.destroy()

def deleteChildren(parent):
    if len(parent.winfo_children()) > 0:
        for child in parent.winfo_children():
            deleteChildren(child)
    else:
        parent.destroy()

# Menu
frame = tkinter.ttk.Frame(container)
frame.grid(row=0, column=0, sticky='nsew')
label = tkinter.ttk.Label(frame, text="Select Mode")
label.pack(side='top', ipady=40)
label2 = tkinter.ttk.Label(frame,text="Please check washer/dryer that its ready to use.")
#add a comment to test git
label2.pack(side='top')
button = tkinter.ttk.Button(frame, text="Wash", command=genchoice('wash'))
button.pack(side='left', expand=1, fill='both')
button = tkinter.ttk.Button(frame, text="Dry", command=genchoice('dry'))
button.pack(side='left', expand=1, fill='both')

# Start UI loop
root.mainloop()
