"""Database layer to NT Service."""
import logging
import time

import pyodbc  # type: ignore

from . import config

logger = logging.getLogger(__name__)

_conn = pyodbc.connect(config.dbconn)
_resultserror = ["fail to deduct",
                 None,
                 "no enough value",
                 "card blocked",
                 "no card found",
                 "card value blocked Problem",
                 "Card Problem",
                 "Too slow"]  # self-defined


def pay(amount: float):
    """Pay using NT Service."""
    cursor = _conn.cursor()
    logging.debug("insert order")
    cursor.execute("DELETE FROM OrderDetail")  # delete existing orders
    cursor.execute("""
                   INSERT INTO OrderDetail VALUES
                   ('Laundry', 1, ?, NULL),
                   ('END$', 0, 0, NULL)
                   """, amount)
    _conn.commit()

    for n in range(1, 31):
        logging.debug(f"check result {n}")
        cursor.execute("""
                       SELECT CardID, Amount, ResultStatus, ResultDateTime
                        FROM Deductionresult
                       """)
        try:
            cardid, amountleft, status, datetime = next(cursor)
            logger.debug(f"order result {cardid=} {amountleft=} {status=}"
                         f" {datetime=}")
            break
        except StopIteration:
            time.sleep(1)
    else:
        status = -1
        logger.debug("Too slow")
        cursor.execute("DELETE FROM OrderDetail")  # clean up order
        _conn.commit()

    cursor.execute("""DELETE FROM Deductionresult""")
    logger.debug("delete result")
    _conn.commit()

    if error := _resultserror[status]:
        raise ValueError(error)
